<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
class RegisterController extends AppController
{

    public function plan(){
        $this->current_pricing = "current-menu-item";
        $this->register_plan = "current-menu-item";
    }


    public function configure($plan_id=null){
        $this->current_pricing = "current-menu-item";
        $this->register_configure = "current-menu-item";
        $this->plan_id = $plan_id;
        $this->list_genres = Load::model("genre")->find();
    }

    public function join(){
        $this->current_pricing = "current-menu-item";
        $this->register_join = "current-menu-item";

        $this->plan_id = null;
        $this->genres = null;

        if (Input::is('POST')){
            $this->plan_id = Input::post('plan_id');
            $this->genres = Input::post('genres');
        }
    }

    public function register(){
        $this->current_pricing = "current-menu-item";

        if (Input::is('POST')){
            
            if (Input::hasPost('name') && Input::hasPost('email') &&
                Input::hasPost('password') && Input::hasPost('confirm-password')){

                Load::model("Users");
                $user = new Users(array(
                    'name' => Input::post('name'),
                    'email' => Input::post('email'),
                    'plan_id' => Input::post('plan_id'),
                    'hashed_password' => sha1(Input::post('password')),
                ));
                $user->create_from_request();

                if (Input::hasPost('genres')){
                    Load::model('users_genres');
                    $genres = explode(",", Input::post('genres'));
                    if (is_array($genres) && count($genres) > 0){
                        foreach ($genres as $key => $value) {
                            $obj = new UsersGenres(array(
                                'users_id' => $user->id,
                                'genre_id' => $value
                            ));
                            $obj->save();
                        }
                    }
                }
                Redirect::to("register/welcome");
            }
        }
    }

    public function welcome(){
        $this->current_pricing = "current-menu-item";
    }

}
