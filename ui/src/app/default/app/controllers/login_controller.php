<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */

Load::model("users");
class LoginController extends AppController
{

    public function index(){
        if (Input::is('POST')){
            $login = DwAuth::login(array('email' => Input::post('login')), array('hashed_password' => Input::post('password')));
            if($login === true) {    
                return Redirect::to("dashboard");
            }
        }
        return Redirect::to("index");
    }


    public function logout(){
        DwAuth::logout();
        return Redirect::toAction('/');
    }
}
