<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
class DashboardController extends AdminController
{


	protected function before_filter() {
        View::template('backend');
        $this->list_genres = Load::model("genre")->find();
        $this->list_countries = Load::model("country")->find();

        $this->users_genres = Load::model("users_genres");

        $this->list_genres_user = $this->users_genres->getGenreUser(Session::get('id'));

        $this->first_genre = $this->users_genres->getGenre(Session::get('id'));

        if (is_null($this->first_genre)){
            $this->first_genre = new stdClass();
            $this->first_genre->id = 0;
            $this->first_genre->name = '-';
        }
    }

    public function popularity($page = 1){

        $this->select_sidebar_nav_popular = 'current-menu-item';
        $this->movies = Load::model("movie")->getAllPopular(Session::get('id'), $page);
        $this->section_demograph = "Most Popular";
        $this->path = "/popularity";
        View::select('movies');
    }


    public function toprated($page_number = 1){

        $movie = Load::model("movie");

        $this->select_sidebar_nav_top_rated = 'current-menu-item';
        $this->section_demograph = "Top Rated Popular";
        $this->path = "/toprated";

        $api = Load::model('api');
        $per_page = 12;

        $page_number = isset($page_number) ? (int) $page_number : 1;
        $start = $per_page * ($page_number - 1);

        $movies_related = $api->getTopRatedMovies($start, $per_page);
        $this->related = [];

        if (isset($movies_related['movies']) && is_array($movies_related['movies']) && count($movies_related['movies']) > 0){

            $this->related = $movie->getMoviesByDataId($movies_related['movies'], Session::get('id'));

            $start = $per_page * ($page_number - 1);

            $this->movies = new stdClass();
            $this->movies->next = ($start + $per_page) < $n ? ($page_number + 1) : false;
            $this->movies->prev = ($page_number > 1) ? ($page_number - 1) : false;
            $this->movies->current = $page_number;
            $this->movies->total = ceil($movies_related['total'] / $per_page);
            $this->movies->count = $this->related['count'];
            $this->movies->per_page = $per_page;
            $this->movies->items = $this->related;
        }


        View::select('movies');
    }
    

    public function index(){
    	$this->select_sidebar_nav = 'current-menu-item';
        $this->movies = Load::model("movie")->getAll($this->first_genre->id, 1, Session::get('id'));
        $this->index = true;
        $this->path = "/movies/{$this->first_genre->id}";
    }


    public function profile(){

        $this->current_item_profile = "current";

        $movie = Load::model("movie");
        $this->movies_watched = Load::model("users_movies")->countMoviesWatched(Session::get('id'));
        $this->movies_favorites = Load::model("users_movies")->countMoviesFavorities(Session::get('id'));

        $api = Load::model('api');
        $movies_related = $api->getColaborativeFilteringMovies();
        $this->related = [];
        if (isset($movies_related) && is_array($movies_related) && count($movies_related) > 0){
            $this->related = $movie->getMoviesByDataId($movies_related, Session::get('id'));
        }
    }


    public function movies($genre_id, $page = 1){
        $this->select_sidebar_nav = 'current-menu-item';
        $this->movies = Load::model("movie")->getAll($genre_id, $page, Session::get('id'));
        $this->first_genre = $this->users_genres->getGenre(Session::get('id'), $genre_id);
        $this->index = true;
        $this->path = "/movies/{$this->first_genre->id}";
        
    }


    public function favorities(){
        $users_movies = Load::model("users_movies");
        $this->movies_watched = $users_movies->countMoviesWatched(Session::get('id'));
        $this->movies_favorites = Load::model("users_movies")->countMoviesFavorities(Session::get('id'));
        $this->select_sidebar_nav = 'current-menu-item';
        $this->current_item_profile_favorites = "current";
        $this->related = $users_movies->getAllFavorities(Session::get('id'), $page);
        View::select('profile');
        

    }

    public function movie($movie_id){
        $movie = Load::model("movie");

        $this->movie = $movie->findMovie($movie_id, Session::get('id'));

        $api = Load::model('api');
        $movies_related = [];
        $movies_related = $api->getContentBaseMovies($this->movie->title);
        $this->related = [];

        if (isset($movies_related) && is_array($movies_related) && count($movies_related) > 0){
            $this->related = $movie->getMoviesByDataId($movies_related, Session::get('id'));
        }

    }   



    public function favorite(){

        View::select(null, null);
        $this->data = array('success' => false);

        if (Input::isAjax() === true){
            if(Input::hasPost('movie_id')) {
                $movie_id = Input::post('movie_id');
                $users_movie = Load::model('users_movies');
                $user_id = Session::get('id');

                $exists = $users_movie->find_first("conditions: users_id = {$user_id} AND movie_id = {$movie_id}");

                try {
                    if ($exists === false){
                        $users_movie = new UsersMovies(array(
                            'users_id' => $user_id,
                            'movie_id' => $movie_id,
                            'favorite' => true
                        ));
                        $users_movie->save();
                    }else{
                        $users_movie = new UsersMovies(array(
                            'id' => $exists->id,
                            'users_id' => $user_id,
                            'movie_id' => $movie_id,
                            'favorite' => true
                        ));
                        $users_movie->save();
                    }
                    $this->data = array('success' => true);
                } catch (Exception $e) {
                    
                }
                
            }
        }
        View::select(NULL, 'json');
    }


    public function watch(){

        View::select(null, null);
        $this->data = array('success' => false);

        if (Input::isAjax() === true){
            if(Input::hasPost('movie_id')) {
                $movie_id = Input::post('movie_id');
                $users_movie = Load::model('users_movies');
                $user_id = Session::get('id');
                $exists = $users_movie->find_first("conditions: users_id = {$user_id} AND movie_id = {$movie_id}");

                try {
                    if ($exists === false){
                        $users_movie = new UsersMovies(array(
                            'users_id' => $user_id,
                            'movie_id' => $movie_id,
                            'watched' => true
                        ));
                        $users_movie->save();
                    }else{
                        $users_movie = new UsersMovies(array(
                            'id' => $exists->id,
                            'users_id' => $user_id,
                            'movie_id' => $movie_id,
                            'watched' => true
                        ));
                        $users_movie->save();
                    }
                    $this->data = array('success' => true);
                } catch (Exception $e) {
                    
                }
                
            }
        }
        View::select(NULL, 'json');
    }


    public function rate(){

        View::select(null, null);
        $this->data = array('success' => false);

        if (Input::isAjax() === true){
            if(Input::hasPost('movie_id') && Input::hasPost('rate')) {
                $movie_id = Input::post('movie_id');
                $data_id = Input::post('data_id');
                $rate = Input::post('rate');

                $users_movie = Load::model('users_movies');
                $user_id = Session::get('id');
                $exists = $users_movie->find_first("conditions: users_id = {$user_id} AND movie_id = {$movie_id}");

                try {
                    if ($exists === false){
                        $users_movie = new UsersMovies(array(
                            'users_id' => $user_id,
                            'movie_id' => $movie_id,
                            'rate' => $rate
                        ));
                        $users_movie->save();
                        $this->data = array('success' => true);
                    }else{
                        $users_movie = new UsersMovies(array(
                            'id' => $exists->id,
                            'users_id' => $user_id,
                            'movie_id' => $movie_id,
                            'rate' => $rate
                        ));
                        $users_movie->save();
                    }
                    $this->data = array('success' => true);
                } catch (Exception $e) {
                    
                }

                $ratings = Load::model('ratings');
                $exists = $ratings->find_first("conditions: userid = {$user_id} AND movieid = {$data_id}");

                try {
                    if ($exists === false){
                        $ratings = new Ratings(array(
                            'userid' => $user_id,
                            'movieid' => $data_id,
                            'rating' => ($rate / 2),
                            'timestamp' => time()
                        ));
                        $ratings->save();
                        $this->data = array('success' => true);
                    }else{
                        $ratings = new Ratings(array(
                            'id' => $exists->id,
                            'userid' => $user_id,
                            'movieid' => $data_id,
                            'rating' => ($rate / 2),
                            'timestamp' => time()
                        ));
                        $ratings->save();
                    }
                    $this->data = array('success' => true);
                } catch (Exception $e) {
                    
                }
            }
        }
        View::select(NULL, 'json');
    }

    
}
