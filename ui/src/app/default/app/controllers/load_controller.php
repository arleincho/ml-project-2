<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
Load::model("company");
Load::model("country");
Load::model("genre");
Load::model("keyword");
Load::model("language");
Load::model("movie");
Load::model("movie_company");
Load::model("movie_country");
Load::model("movie_genre");
Load::model("movie_language");
Load::model("movie_keyword");
Load::model("users");


ini_set('max_execution_time', 3000);

class LoadController extends AppController
{

    public function index(){

        View::select('');

        $genres = array();
        $keywords = array();
        $companies = array();
        $countries = array();
        $languages = array();

        
        $user = new Users();

        $user->sql("truncate table public.company");
        $user->sql("truncate table public.country");
        $user->sql("truncate table public.genre");
        $user->sql("truncate table public.keyword");
        $user->sql("truncate table public.language");
        $user->sql("truncate table public.movie");
        $user->sql("truncate table public.movie_company");
        $user->sql("truncate table public.movie_country");
        $user->sql("truncate table public.movie_genre");
        $user->sql("truncate table public.movie_language");
        $user->sql("truncate table public.movie_keyword");

        $poster_big_img = array(
            PUBLIC_PATH . 'img/dashboard/movies/Robot-2.0-Movie-New-Poster-Stills5.jpg',
            PUBLIC_PATH . 'img/dashboard/movies/dashboard-movie-poster.jpg',
            PUBLIC_PATH . 'img/dashboard/movies/medium-poster8898-wall-poster-movies-the-matrix-wall-poster-original-imaf5tgtzvb8pdpc.jpeg',
        );

        $poster_small_img = array(
            PUBLIC_PATH . 'img/dashboard/movies/ant_man_ver5.jpg',
            PUBLIC_PATH . 'img/dashboard/movies/movie-detail-poster-1.jpg',
            PUBLIC_PATH . 'img/dashboard/movies/movie-detail-poster.jpg',
        );

        $poster_list_img = array(
            PUBLIC_PATH . 'img/dashboard/list/listing-1.jpg',
            PUBLIC_PATH . 'img/dashboard/list/listing-2.jpg',
            PUBLIC_PATH . 'img/dashboard/list/listing-3.jpg',
            PUBLIC_PATH . 'img/dashboard/list/listing-4.jpg',
            PUBLIC_PATH . 'img/dashboard/list/listing-5.jpg',
            PUBLIC_PATH . 'img/dashboard/list/listing-6.jpg',
            PUBLIC_PATH . 'img/dashboard/list/listing-10.jpg',
            PUBLIC_PATH . 'img/dashboard/list/listing-12.jpg'
        );

        
        $row = 0;
        if (($handle = fopen("/data/tmdb_5000_movies.csv", "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {

                if ($row > 0){

                    $num = count($data);
                    $movie_genres = array();
                    $movie_keywords = array();
                    $movie_companies = array();
                    $movie_countries = array();
                    $movie_languages = array();
                    

                    for ($c=0; $c < $num; $c++) {
                        switch ($c) {
                            case 1:
                                $j_genres = json_decode($data[$c], true);
                                if (is_array($j_genres) && count($j_genres) > 0){
                                    for ($i=0; $i < count($j_genres); $i++) { 
                                        if (!isset($genres[$j_genres[$i]['name']])){
                                            $obj = new Genre(array(
                                                'data_id' => $j_genres[$i]['id'],
                                                'name' => $j_genres[$i]['name'],
                                                'icon' => strtolower(preg_replace("/\'/", " ", $$j_genres[$i]['name'])) . ".png"
                                            ));
                                            $obj->save();
                                            $genres[$j_genres[$i]['name']] = $obj->id;
                                        }
                                        $movie_genres[] = $genres[$j_genres[$i]['name']];
                                    }
                                }
                            break;

                            case 4:
                                $j_keywords = json_decode($data[$c], true);

                                if (is_array($j_keywords) && count($j_keywords) > 0){
                                    for ($i=0; $i < count($j_keywords); $i++) { 

                                        if (!isset($keywords[$j_keywords[$i]['name']])){
                                            $obj = new Keyword(array(
                                                'data_id' => $j_keywords[$i]['id'],
                                                'name' => preg_replace("/\'/", " ", $j_keywords[$i]['name'])
                                            ));
                                            $obj->save();
                                            $keywords[$j_keywords[$i]['name']] = $obj->id;
                                        }
                                        $movie_keywords[] = $keywords[$j_keywords[$i]['name']];
                                    }
                                }

                            break;
                            
                            case 9:
                                $j_companies = json_decode($data[$c], true);

                                if (is_array($j_companies) && count($j_companies) > 0){
                                    for ($i=0; $i < count($j_companies); $i++) { 

                                        if (!isset($companies[$j_companies[$i]['name']])){
                                            $obj = new Company(array(
                                                'data_id' => $j_companies[$i]['id'],
                                                'name' => preg_replace("/\'/", " ", $j_companies[$i]['name'])
                                            ));
                                            $obj->save();
                                            $companies[$j_companies[$i]['name']] = $obj->id;
                                        }
                                        $movie_companies[] = $companies[$j_companies[$i]['name']];
                                    }
                                }


                            break;
                            
                            case 10:
                                $j_countries = json_decode($data[$c], true);

                                if (is_array($j_countries) && count($j_countries) > 0){
                                    for ($i=0; $i < count($j_countries); $i++) { 

                                        if (!isset($countries[$j_countries[$i]['name']])){
                                            $obj = new Country(array(
                                                'iso_3166_1' => $j_countries[$i]['iso_3166_1'],
                                                'name' => preg_replace("/\'/", " ", $j_countries[$i]['name'])
                                            ));
                                            $obj->save();
                                            $countries[$j_countries[$i]['name']] = $obj->id;
                                        }
                                        $movie_countries[] = $countries[$j_countries[$i]['name']];
                                    }
                                }

                            break;
                            
                            case 14:
                                $j_language = json_decode($data[$c], true);

                                if (is_array($j_language) && count($j_language) > 0){
                                    for ($i=0; $i < count($j_language); $i++) { 

                                        if (!isset($languages[$j_language[$i]['name']])){
                                            $obj = new Language(array(
                                                'iso_639_1' => $j_language[$i]['iso_639_1'],
                                                'name' => preg_replace("/\'/", " ", $j_language[$i]['name'])
                                            ));
                                            $obj->save();
                                            $languages[$j_language[$i]['name']] = $obj->id;
                                        }
                                        $movie_languages[] = $languages[$j_language[$i]['name']];
                                    }
                                }

                            break;
                        }
                    }

                    $release_date = ($data[11] != '')?$data[11]:date("Y-m-d H:i:s");

                    $poster_big_img_rnd = array_rand($poster_big_img, 1);
                    $poster_small_img_rnd = array_rand($poster_small_img, 1);
                    $poster_list_img_rnd = array_rand($poster_list_img, 1);


                    $objMovie = new Movie(array(
                        'budget' => $data[0],
                        'homepage' => preg_replace("/\'/", "", $data[2]),
                        'data_id' => $data[3],
                        'original_language' => $data[5],
                        'original_title' => preg_replace("/\'/", "", $data[6]),
                        'overview' => preg_replace("/\'/", "", $data[7]),
                        'popularity' => $data[8],
                        'release_date' => date("m/d/y", strtotime($release_date)),
                        'revenue' => $data[12],
                        'runtime' => $data[13],
                        'status' => $data[15],
                        'tagline' => preg_replace("/\'/", "", $data[16]),
                        'title' => preg_replace("/\'/", "", $data[17]),
                        'vote_average' => $data[18],
                        'vote_count' => $data[19],
                        'poster_big' => $poster_big_img[$poster_big_img_rnd],
                        'poster_small' => $poster_small_img[$poster_small_img_rnd],
                        'poster_list' => $poster_list_img[$poster_list_img_rnd],
                    ));
                    
                    try {
                        $objMovie->save();

                        foreach ($movie_genres as $key => $value) {
                            $obj = new MovieGenre(array(
                                'movie_id' => $objMovie->id,
                                'genre_id' => $value
                            ));
                            $obj->save();
                        }

                        foreach ($movie_keywords as $key => $value) {
                            $obj = new MovieKeyword(array(
                                'movie_id' => $objMovie->id,
                                'keyword_id' => $value
                            ));
                            $obj->save();
                        }


                        foreach ($movie_companies as $key => $value) {
                            $obj = new MovieCompany(array(
                                'movie_id' => $objMovie->id,
                                'company_id' => $value
                            ));
                            $obj->save();
                        }


                        foreach ($movie_countries as $key => $value) {
                            $obj = new MovieCountry(array(
                                'movie_id' => $objMovie->id,
                                'country_id' => $value
                            ));
                            $obj->save();
                        }

                        
                        foreach ($movie_languages as $key => $value) {
                            $obj = new MovieLanguage(array(
                                'movie_id' => $objMovie->id,
                                'language_id' => $value
                            ));
                            $obj->save();
                        }

                    } catch (Exception $e) {}
                }
                $row++;

            }
            fclose($handle);
        }
    }
}






