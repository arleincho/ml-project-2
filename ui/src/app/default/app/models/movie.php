<?php
/**
 *
 * Clase que gestiona los menús de los usuarios según los recursos asignados
 *
 * @category
 * @package     Models
 * @subpackage
 */

class Movie extends ActiveRecord {

	function getAll($genre_id = null, $page, $user_id){

		$join = "";
		if(is_null($genre_id) === false){
			$join .= "INNER JOIN movie_genre ON movie_genre.genre_id = {$genre_id} AND movie.id = movie_genre.movie_id";
		}

		$join .= " LEFT JOIN users_movies ON users_movies.movie_id = movie.id AND users_movies.users_id = {$user_id} ";

        $columnas = 'movie.*, users_movies.rate';
        return $this->paginate("columns: $columnas", "join: $join", "page: $page", 'per_page: 12', 'order: id desc');
	}


	function getAllPopular($user_id, $page){
		$join = " INNER JOIN movie_genre ON  movie.id = movie_genre.movie_id ";
		$join .= " INNER JOIN users_genres ON  users_genres.users_id = {$user_id} AND users_genres.genre_id = movie_genre.movie_id ";
        $columnas = 'movie.*, users_movies.rate';

        $sql = "SELECT movie.*, users_movies.rate
        	FROM movie
        	LEFT JOIN users_movies ON users_movies.movie_id = movie.id AND users_movies.users_id = {$user_id}
        	WHERE movie.id IN(
				SELECT movie_genre.movie_id
				FROM users_genres
					INNER JOIN movie_genre ON movie_genre.genre_id = users_genres.genre_id
				WHERE users_genres.users_id = {$user_id}
				GROUP BY movie_genre.movie_id)
			ORDER BY popularity desc";

        return $this->paginate_by_sql($sql, "page: $page", 'per_page: 12');
	}


	function findMovie($movie_id, $user_id){
		$movie = Filter::get($movie_id, 'int');
        if(!$movie) {
            return NULL;
        }
	    $columnas = 'movie.*, users_movies.rate';
        $join = "LEFT JOIN users_movies ON users_movies.movie_id = movie.id AND users_movies.users_id = {$user_id}";
        $condicion = "movie.id = {$movie}";
        return $this->find_first("columns: $columnas", "join: $join", "conditions: $condicion");
	}


	function getMoviesByDataId($movies, $user_id){
		
		if (is_array($movies) && count($movies) > 0){
			$columnas = 'movie.*, users_movies.rate';
        	$join = "LEFT JOIN users_movies ON users_movies.movie_id = movie.id AND users_movies.users_id = {$user_id}";

			$movies_data_id = implode(",", $movies);
			return $this->find("columns: $columnas", "join: $join", "conditions: data_id IN ({$movies_data_id})");
		}
		return null;

	}
}