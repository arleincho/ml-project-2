<?php
/**
 *
 * Clase que gestiona los menús de los usuarios según los recursos asignados
 *
 * @category
 * @package     Models
 * @subpackage
 */

class UsersGenres extends ActiveRecord {


	public function getGenreUser($user_id) {
        $usuario = Filter::get($user_id, 'int');
        if(!$usuario) {
            return NULL;
        }
        $columnas = 'genre.name, genre.data_id, genre.icon, genre.id';
        $join = 'INNER JOIN genre ON genre.id = users_genres.genre_id ';
        $condicion = "users_genres.users_id = $usuario";        
        return $this->find("columns: $columnas", "join: $join", "conditions: $condicion");
    }

    public function getGenre($user_id, $gender_id = null) {
        $user = Filter::get($user_id, 'int');
        $gender = Filter::get($gender_id, 'int');
        if(!$user) {
            return NULL;
        }

        // $condition
        $columnas = 'genre.name, genre.data_id, genre.id, genre.icon';
        $join = 'INNER JOIN genre ON genre.id = users_genres.genre_id ';
        $condicion = "users_genres.users_id = $user";
        if($gender) {
            $condicion .= " AND genre.id = {$gender}"; 
        }
        return $this->find_first("columns: $columnas", "join: $join", "conditions: $condicion");
    }
}