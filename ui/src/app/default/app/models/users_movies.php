<?php
/**
 *
 * Clase que gestiona los menús de los usuarios según los recursos asignados
 *
 * @category
 * @package     Models
 * @subpackage
 */

class UsersMovies extends ActiveRecord {


	function countMoviesWatched($user_id){

		$usuario = Filter::get($user_id, 'int');
        if(!$usuario) {
            return NULL;
        }
        $condicion = "users_movies.users_id = $usuario AND watched IS TRUE";
        return $this->count("conditions: $condicion");
    }

    function countMoviesFavorities($user_id){

        $usuario = Filter::get($user_id, 'int');
        if(!$usuario) {
            return NULL;
        }
        $condicion = "users_movies.users_id = $usuario AND favorite IS TRUE";
        return $this->count("conditions: $condicion");
    }


    function getAllFavorities($user_id){

        $columnas = 'movie.*, users_movies.rate';
        $join .= "INNER JOIN movie ON users_movies.movie_id = movie.id";
        $conditions = " users_movies.users_id = {$user_id} AND favorite IS TRUE ";

        return $this->find("columns: $columnas", "conditions: {$conditions}", "join: $join");
	}

}