<?php
/**
 *
 * Clase que gestiona los menús de los usuarios según los recursos asignados
 *
 * @category
 * @package     Models
 * @subpackage
 */

require_once APP_PATH . 'libs/JJG/Request.php';

use JJG\Request as Request;

class Api{

	public function getContentBaseMovies($title){

		$request = new Request('http://api:8888/recomendation/content-base/');
		$request->setRequestType('POST');
		$request->setPostFields(array('title' => $title));
		$request->setTimeout(60);
 		$request->execute();
 		$movies = [];

 		if ($request->getHttpCode() == 200){
 			$response = json_decode($request->getResponse(), true);

 			if (isset($response['movies']) && count($response['movies']) > 0){
 				$movies = $response['movies'];
 			}
 		};
 		return $movies;
	}


	public function getColaborativeFilteringMovies(){

		$request = new Request('http://api:8888/recomendation/collaborative-filtering/');
		$request->setRequestType('POST');
		$request->setPostFields(array('userid' => Session::get('id'), 'limit' => 9));
		$request->setTimeout(60);
 		$request->execute();
 		$movies = [];

 		if ($request->getHttpCode() == 200){
 			$response = json_decode($request->getResponse(), true);

 			if (isset($response['movies']) && count($response['movies']) > 0){
 				$movies = $response['movies'];
 			}
 		};
 		return $movies;
	}


	public function getDemographicsMovies(){

		$request = new Request('http://api:8888/recomendation/collaborative-filtering/');
		$request->setRequestType('POST');
		$request->setPostFields(array('userid' => Session::get('id'), 'limit' => 12));
		$request->setTimeout(60);
 		$request->execute();
 		$movies = [];

 		if ($request->getHttpCode() == 200){
 			$response = json_decode($request->getResponse(), true);

 			if (isset($response['movies']) && count($response['movies']) > 0){
 				$movies = $response['movies'];
 			}
 		};
 		return $movies;
	}


	public function getTopRatedMovies($offset, $limit){

		$request = new Request('http://api:8888/recomendation/toprated/');
		$request->setRequestType('POST');
		$request->setPostFields(array(
			'offset' => $offset,
			'limit' => $limit
		));
		$request->setTimeout(60);
 		$request->execute();
 		$movies = [];

 		if ($request->getHttpCode() == 200){
 			$response = json_decode($request->getResponse(), true);

 			if (isset($response['movies']) && count($response['movies']) > 0){
 				return $response;
 			}
 		};
 		return $movies;
	}
}