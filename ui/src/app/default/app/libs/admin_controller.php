<?php
/**
 * @see Controller nuevo controller
 */
require_once CORE_PATH . 'kumbia/controller.php';

/**
 * Controlador para proteger los controladores que heredan
 * Para empezar a crear una convención de seguridad y módulos
 *
 * Todas las controladores heredan de esta clase en un nivel superior
 * por lo tanto los métodos aquí definidos estan disponibles para
 * cualquier controlador.
 *
 * @category Kumbia
 * @package Controller
 */
class AdminController extends Controller
{

    final protected function initialize()
    {
        //Código de auth y permisos
        //Será libre, pero añadiremos uno por defecto en breve
        //Posiblemente se cree una clase abstracta con lo que debe tener por defecto
    	if( !DwAuth::isLogged() ) {
            // El usuario no es valido, lo mandamos al login
            if( ($this->controller_name != 'login') && ( $this->action_name != 'index' && $this->action_name != 'salir') ) {
            	Redirect::to("index");
            }
            return false;
        } else if( DwAuth::isLogged() && $this->controller_name!='login' ) {
            if($this->controller_name != 'dashboard'){
                Redirect::to('dashboard');
            }
        }
    }


    final protected function finalize()
    {
        
    }

}
