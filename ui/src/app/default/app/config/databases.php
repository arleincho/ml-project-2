<?php
/**
 * KumbiaPHP Web Framework
 * Parámetros de conexión a la base de datos
 */
return [

    'production' => [
        /**
         * host: ip o nombre del host de la base de datos
         */
        'host'     => 'postgres',
        /**
         * username: usuario con permisos en la base de datos
         */
        'username' => 'api', //no es recomendable usar el usuario root
        /**
         * password: clave del usuario de la base de datos
         */
        'password' => 'api',
        /**
         * test: nombre de la base de datos
         */
        'name'     => 'api',
        /**
         * type: tipo de motor de base de datos (mysql, pgsql o sqlite)
         */
        'type'     => 'pgsql',
        /**
         * charset: Conjunto de caracteres de conexión, por ejemplo 'utf8'
         */
        'charset'  => 'utf8',
        /**
         * dsn: cadena de conexión a la base de datos
         */
        'dsn' => sprintf("host=%s;port=%d;dbname=%s", "postgres", 5432, "api"),
        /**
         * pdo: activar conexiones PDO (OnOff); descomentar para usar
         */
        'pdo' => 'On',
        ],
];
