#!/usr/bin/env python3
#
# Copyright 2009 Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import aiopg
import markdown
import os.path
import psycopg2
import re
import tornado.escape
import tornado.httpserver
import tornado.ioloop
import tornado.locks
import tornado.web
import pandas as pd
import pickle
import json
import content_based
import collaborative_filtering


from surprise import SVD
from surprise.dataset import Dataset
from surprise.reader import Reader

db_host = os.environ['POSTGRES_HOST']
db_user = os.environ['POSTGRES_USER']
db_password = os.environ['POSTGRES_PASSWORD']
db_port = os.environ['POSTGRES_PORT']
db_database = os.environ['POSTGRES_DB']

api_port = os.environ['API_PORT']

token_type = "MachineLearning"


class NoResultError(Exception):
    pass


class Application(tornado.web.Application):
    def __init__(self, db):

        self.db = db
        handlers = [
            (r"/recomendation/content-base/", ContentBaseHandler),
            (r"/recomendation/collaborative-filtering/", ColaborativeFilteringBaseHandler),
            (r"/recomendation/toprated/", TopRatedeHandler)
        ]
        settings = dict(
            cookie_secret="Movb9mdCSSCjI3SoOjNOrcRoXhfZdz04RqFJiUdDWsp3EOPvjVTOYkHyR5exwP5Z22rpM",
            debug=True,
        )
        super(Application, self).__init__(handlers, **settings)


class BaseHandler(tornado.web.RequestHandler):

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Credentials", "true")
        self.set_header("Access-Control-Allow-Headers", "cache-control,Content-Type,X-Amz-Date,Authorization,X-Api-Key,Origin,Accept,Access-Control-Allow-Headers,Access-Control-Allow-Methods,Access-Control-Allow-Origin")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')


    def row_to_obj(self, row, cur):
        """Convert a SQL row to an object supporting dict and attribute access."""
        obj = tornado.util.ObjectDict()
        for val, desc in zip(row, cur.description):
            obj[desc.name] = val
        return obj

    async def options(self):
        # no body
        self.set_status(200)
        return


    async def execute(self, stmt, *args):
        """Execute a SQL statement.

        Must be called with ``await self.execute(...)``
        """
        with (await self.application.db.cursor()) as cur:
            await cur.execute(stmt, args)

    async def query(self, stmt, *args):
        """Query for a list of results.

        Typical usage::

            results = await self.query(...)

        Or::

            for row in await self.query(...)
        """
        with (await self.application.db.cursor()) as cur:
            await cur.execute(stmt, args)
            return [self.row_to_obj(row, cur) for row in await cur.fetchall()]

    async def queryone(self, stmt, *args):
        """Query for exactly one result.

        Raises NoResultError if there are no results, or ValueError if
        there are more than one.
        """
        results = await self.query(stmt, *args)
        if len(results) == 0:
            raise NoResultError()
        elif len(results) > 1:
            raise ValueError("Expected 1 result, got %d" % len(results))
        return results[0]


  
class ContentBaseHandler(BaseHandler):

    async def post(self):
        try:
            params = { k: self.get_argument(k) for k in self.request.arguments }

            movies = content_based.getRecommendations(params['title'])
            print("estamos recibiendo %s:: y el resultado es: %s" % (params, movies))

            self.write({"movies": movies})

        except NoResultError:
            raise tornado.web.HTTPError(401)
            return

    
class ColaborativeFilteringBaseHandler(BaseHandler):

    async def post(self):
        try:
            params = { k: self.get_argument(k) for k in self.request.arguments }

            ratings = await self.query('SELECT userid, movieid AS id, rating FROM ratings inner join movie on ratings.movieid = movie.data_id')

            users = pd.DataFrame(ratings)
            reader = Reader(rating_scale=(1, 5)) 
            data = Dataset.load_from_df(users, reader=reader)
            trainset =  data.build_full_trainset()

            algoSVD = SVD(n_factors=125, n_epochs=70, lr_all=0.005, reg_all=0.4)
            algoSVD.fit(trainset)

            recommendations = collaborative_filtering.getRecommendations(algoSVD, params['userid'], users, params['limit'])
            self.write({"movies": recommendations.values.tolist()})

        except NoResultError:
            raise tornado.web.HTTPError(401)
            return


def weighted_rating(x, m, C):
    v = x['vote_count']
    R = x['vote_average']
    # Calculation based on the IMDB formula
    return (v/(v+m) * R) + (m/(m+v) * C)
  
    
class TopRatedeHandler(BaseHandler):

    async def post(self):
        try:
            params = { k: self.get_argument(k) for k in self.request.arguments }

            movies = await self.query('SELECT data_id, vote_count, vote_average FROM movie')

            df_original = pd.DataFrame(movies)

            C = df_original['vote_average'].mean()
            m = df_original['vote_count'].quantile(0.75)

            df_byScore = df_original.copy().loc[df_original['vote_count'] >= m]
            df_byScore['score'] = df_byScore.apply(weighted_rating, args=(m, C), axis=1)
            df_byScore.sort_values('score', ascending=False)

            offset = int(params['offset']);
            limit = int(params['limit']) + offset

            self.write({"movies": df_byScore['data_id'].iloc[offset:limit].values.tolist(), 'total': len(df_byScore.index)})

        except NoResultError:
            raise tornado.web.HTTPError(401)
            return

  

async def main():

    # Create the global connection pool.
    async with aiopg.create_pool(
        host=db_host,
        port=db_port,
        user=db_user,
        password=db_password,
        dbname=db_database,
    ) as db:
        app = Application(db)
        app.listen(api_port)

        # In this demo the server will simply run until interrupted
        # with Ctrl-C, but if you want to shut down more gracefully,
        # call shutdown_event.set().
        shutdown_event = tornado.locks.Event()
        await shutdown_event.wait()


if __name__ == "__main__":
    tornado.ioloop.IOLoop.current().run_sync(main)
