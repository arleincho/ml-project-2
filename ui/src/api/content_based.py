import pickle
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity



def loadPicklesFiles():

    pickle_in = open('/pickles/response.pickle','rb')
    response = pickle.load(pickle_in)

    pickle_in = open('/pickles/df_Genre.pickle','rb')
    df_Genre = pickle.load(pickle_in)

    pickle_in = open('/pickles/df_Actors.pickle','rb')
    df_Actors = pickle.load(pickle_in)

    pickle_in = open('/pickles/df.pickle','rb')
    df = pickle.load(pickle_in)

    pickle_in = open('/pickles/df_OtherScaled.pickle','rb')
    df_OtherScaled = pickle.load(pickle_in)

    return {
        'df': df, 
        'response': response,
        'df_Genre': df_Genre,
        'df_Actors': df_Actors,
        'df_OtherScaled': df_OtherScaled
    }



def getRecommendations(Movie):

    preprocessing = loadPicklesFiles()

    selectedMovie = Movie
    indexSelectedMovie = preprocessing['df'].index[preprocessing['df']['title'] == selectedMovie][0]
    
    # Text Similarity
    MovieTextSim = []
    for i in range(0,len(preprocessing['df'].index)):
        cos_Sim1 = cosine_similarity(preprocessing['response'][indexSelectedMovie], preprocessing['response'][i])[0][0]
        MovieTextSim.append(cos_Sim1)
    
    # Genre similarity
    vec1 = np.array([preprocessing['df_Genre'].loc[indexSelectedMovie,:].tolist()])
    MovieGenreSim = []
    for i in range(0,len(preprocessing['df'].index)):
        vec2 = np.array([preprocessing['df_Genre'].loc[i,:].tolist()])
        cos_Sim2 = cosine_similarity(vec1, vec2)[0][0]
        MovieGenreSim.append(cos_Sim2)
        
    # Actor similarity 
    vec1 = np.array([preprocessing['df_Actors'].loc[indexSelectedMovie,:].tolist()])
    MovieActorSim = []
    for i in range(0,len(preprocessing['df'].index)):
        vec2 = np.array([preprocessing['df_Actors'].loc[i,:].tolist()])
        cos_Sim3 = cosine_similarity(vec1, vec2)[0][0]
        MovieActorSim.append(cos_Sim3)
        
    # Other Factor Similarity
    vec1 = np.array([preprocessing['df_OtherScaled'].loc[indexSelectedMovie,:].tolist()])
    MovieOtherSim = []
    for i in range(0,len(preprocessing['df'].index)):
        vec2 = np.array([preprocessing['df_OtherScaled'].loc[i,:].tolist()])
        cos_Sim4 = cosine_similarity(vec1, vec2)[0][0]
        MovieOtherSim.append(cos_Sim4)
        
    # Calculating Average Similarity
    AveSim = []
    for i in range(0,len(MovieOtherSim)):
        AVG = 0.5 * MovieTextSim[i] + 0.25 * MovieGenreSim[i] + 0.15 * MovieActorSim[i] + 0.10 * MovieOtherSim[i]
        AveSim.append(AVG)
    arr = np.array(AveSim)
    # top 10 Movies
    indices = arr.argsort()[-11:][::-1] 
    indices = indices[1:]
    RecommendedMovies = []
    for idx in indices:
        RecommendedMovies.append(int(preprocessing['df'].loc[idx,'id']))
    
    return RecommendedMovies

