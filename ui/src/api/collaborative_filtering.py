import pandas as pd
import pickle
from surprise import SVD
from surprise.dataset import Dataset

def update_model(u,i,r): 
    users=pd.read_csv('the-movies-dataset/ratings_small.csv')
    movies=pd.read_csv('tmdb-movie-metadata/tmdb_5000_movies.csv')
    users = users.drop(["timestamp"],axis=1)
    users.columns = ["userid","id","rating"]
    users.append({'userid':u,'id':i,'rating':r}, ignore_index=True)
    users.to_csv("the-movies-dataset/ratings_small.csv", index=False)
    
    reader = Reader(line_format='user item rating', sep=',')

    data = Dataset.load_from_df(users,reader=reader)

    #trainset, testset = train_test_split(data, test_size=.25)
    algoSVD = SVD(n_factors=125, n_epochs=70, lr_all=0.005, reg_all=0.4)
    trainset =  data.build_full_trainset()
    algoSVD.fit(trainset)
    with open('algoSVD.pickle', 'wb') as f:
        pickle.dump(algoSVD, f)

# Function that takes in userId as input and outputs the top10 recommended movies
import pandas as pd
def get_recommendations2(userId):
    users=pd.read_csv('the-movies-dataset/ratings_small.csv')
    movies=pd.read_csv('tmdb-movie-metadata/tmdb_5000_movies.csv')
    users = users.drop(["timestamp"],axis=1)
    users.columns = ["userid","id","rating"]
    pickle_in = open('algoSVD.pickle','rb')
    algoSVD = pickle.load(pickle_in)
    item_prediction_dict = {}
    thisuser = users.loc[users['userid']==userId]
    listmovies = [x for x in users['id'].drop_duplicates() if x not in thisuser['id'].drop_duplicates()]
    for item in listmovies:
        pred = algoSVD.predict(userId, item, verbose=False)
        item_prediction_dict[item]=pred.est

    df_item_predictions = pd.DataFrame.from_dict(item_prediction_dict, orient='index')
    df_item_predictions.reset_index(inplace=True)
    df_item_predictions.rename(index=str, columns={"index":"id" ,0: "rating"}, inplace=True)
    df_top10=df_item_predictions.sort_values(by="rating", ascending=False)[:10]
    uniquemovies = movies.drop_duplicates(subset=['id'])
    df_top10 = df_top10.merge(uniquemovies,on='id')

    # Return the top 10 most similar movies
    #By the way this "id" is the movie id
    return df_top10[["id","title"]]

#This is how to use it, you get a dataframe with the name, id by inputing the userId , in this case 82
# get_recommendations(82)





def getRecommendations(algoSVD, userId, users, limite):

    userId = int(userId)

    item_prediction_dict = {}
    thisuser = users.loc[users['userid'] == userId]
    # listmovies = [x for x in users['id'].drop_duplicates() if x not in thisuser['id'].drop_duplicates()]

    listmovies = users[~users['id'].isin(thisuser['id'])]['id']

    for item in listmovies.values.tolist():
        pred = algoSVD.predict(userId, item, verbose=False)
        item_prediction_dict[item]=pred.est

    df_item_predictions = pd.DataFrame.from_dict(item_prediction_dict, orient='index')
    df_item_predictions.reset_index(inplace=True)
    df_item_predictions.rename(index=str, columns={"index":"id" ,0: "rating"}, inplace=True)
    df_top10=df_item_predictions.sort_values(by="rating", ascending=False)[:int(limite)]

    # Return the top 10 most similar movies
    #By the way this "id" is the movie id
    return df_top10["id"]



