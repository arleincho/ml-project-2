#You can modify the language variable in each case
#There is one case to sort by popularity, another one the the imdb score
#formula, another one by year(most recent first) and finally by genre
import pandas as pd
#Only helper function
df_original=pd.read_csv('tmdb-movie-metadata/tmdb_5000_movies.csv')
C= df_original['vote_average'].mean()
m= df_original['vote_count'].quantile(0.75)
def weighted_rating(x, m=m, C=C):
    v = x['vote_count']
    R = x['vote_average']
    # Calculation based on the IMDB formula
    return (v/(v+m) * R) + (m/(m+v) * C)
#Sort by Score formula 
def getByScore(language,df_original):
    df_byScore = df_original.copy().loc[df_original['vote_count'] >= m] 
    df_byScore =df_byScore.loc[df_byScore['original_language'] == language].copy() 
    df_byScore['score'] = df_byScore.apply(weighted_rating, axis=1)
    return df_byScore.sort_values('score', ascending=False)
#How to use it
language = 'en'
df_byScore1 = getByScore(language,df_original)
df_byScore1[['title', 'vote_count', 'vote_average', 'score']].head(10)

#Sort by popularity
language = 'es'
import matplotlib.pyplot as plt
def getByPopularity(language,df_original):
    df_byPopularity =df_original.loc[df_original['original_language'] == language].copy() 
    return df_byPopularity.sort_values('popularity', ascending=False)
#How to use it
df_original=pd.read_csv('tmdb-movie-metadata/tmdb_5000_movies.csv')
df_byPopularity = getByPopularity(language,df_original)
df_byPopularity[['title', 'vote_count', 'vote_average', 'popularity']].head(10)

#Sort by year
language = 'es'
def getByYear(language, df_original):
    df_byYear = df_original.loc[df_original['original_language'] == language].copy()
    df_byYear.dropna(subset=['release_date'],axis=0)
    df_byYear['year']=pd.to_datetime(df_byYear['release_date']).apply(lambda x: x.date().year*100 +x.date().month)
    return df_byYear.sort_values(['year','popularity'], ascending=False)
#How to use it
df_original=pd.read_csv('tmdb-movie-metadata/tmdb_5000_movies.csv')
df_byYear = getByYear(language, df_original)
df_byYear[['title', 'vote_count', 'vote_average', 'popularity','year']].head(10)

#Sort by genre
import json
def getByGenre(language,df_original,name):
    df_byGenre = df_original.loc[df_original['original_language'] == language].copy()
    df_byGenre['GenresTrue'] = df_byGenre['genres'].apply(lambda genre: name in genre.split('|'))
    df_byGenre = df_byGenre.copy().loc[df_byGenre['GenresTrue'] == True]
    return df_byGenre.sort_values('popularity', ascending=False)

#How to use it
df_original=pd.read_csv('tmdb-movie-metadata/tmdb_5000_movies.csv')
json_columns = ['genres', 'keywords', 'production_countries', 'production_companies', 'spoken_languages']
for column in json_columns:
    df_original[column] = df_original[column].apply(json.loads)
df_original['genres'] = df_original['genres'].apply(lambda genre: '|'.join([x['name'] for x in genre]))    
language = 'es'
#1. select action movies
df_byActionGenre=getByGenre(language,df_original,'Action')
df_byActionGenre[['title', 'vote_count', 'vote_average', 'popularity','genres']].head(10)
#2. select romance movies
df_byRomanceGenre=getByGenre(language,df_original,'Romance')
df_byRomanceGenre[['title', 'vote_count', 'vote_average', 'popularity','genres']].head(10)
#3. select comedy movies
df_byComedyGenre=getByGenre(language,df_original,'Comedy')
df_byComedyGenre[['title', 'vote_count', 'vote_average', 'popularity','genres']].head(10)
